from sklearn import linear_model
import operator
import string
import random
import numpy as np
from sklearn.feature_extraction.text import TfidfVectorizer
import  pickle
from SynonimToolKit import SynonimToolKit
import random

def load_data(filename):
    res = []
    with open(filename, 'r') as f:
        for line in f:
            label, question = line.split(" ", 1)
            res.append((label, question))
    return res

def compute_accuracy(predicted, original):
    eq = [z[0] == z[1] for z in zip(predicted, original)]
    return eq.count(True)/float(len(eq))

training_data_path = "data/train_5500.label"
# training_data_path = "data/test.label"
testing_data_path = "data/TREC_10.label"
vector_dim = 50







train_data = load_data(training_data_path)
test_data = load_data(testing_data_path)

# FF = SynonimToolKit()

# train_questions = [FF.ReplaceSynonym(line[1].lower(),"diseases") for line in train_data]
train_questions = [line[1].lower() for line in train_data]
train_lables = [line[0] for line in train_data]

# print(train_questions)

# random.shuffle(train_questions)


test_questions = [line[1].lower() for line in test_data]
test_lables = [line[0] for line in test_data]



vectorizer =TfidfVectorizer(min_df=1,ngram_range=(1,2),token_pattern=r'\b\w+\b')
question_vectors =vectorizer.fit_transform(train_questions)



f = open('vectorizer.pickle', 'wb')
pickle.dump(vectorizer, f)
f.close()

print (vectorizer.get_feature_names())

#
cfier = linear_model.LogisticRegression(multi_class='multinomial',solver='lbfgs')
cfier.fit(question_vectors, train_lables)

f = open('cfier.pickle', 'wb')
pickle.dump(cfier, f)
f.close()

train_data_prediction = [cfier.predict(vectorizer.transform([line[1].lower()])) for line in train_data]
# test_data_prediction = [cfier.predict(vectorizer.transform([line[1].lower()])) for line in test_data]

print ("Accuracy with fine grained question classes:")
print ("Train accuracy " + str(compute_accuracy(train_data_prediction, train_lables)))
# print ("Test accuracy " + str(compute_accuracy(test_data_prediction, test_lables)))
#



# print(cfier.predict(vectorizer.transform(testquestion)))
