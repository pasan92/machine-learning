import  pickle

f = open('vectorizer.pickle', 'rb')
vectorizer = pickle.load(f)
f.close()

f = open('cfier.pickle', 'rb')
cfier = pickle.load(f)
f.close()

testquestion = ["how to stop"]
print(cfier.predict(vectorizer.transform(testquestion)))


words = []

def load_data():
    newfile = open('data/new.txt','w')
    with open('data/data.txt', 'r') as f:
        for line in f:
            # str =   line.strip('\r\n') + " is a symptom \n"
            str = cfier.predict(vectorizer.transform([line.strip('\r\n')]))[0] +":" + line + '\r\n'
            newfile.writelines(str)
    f.close()
    newfile.close()

load_data()