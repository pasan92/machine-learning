import  pickle
from SynonimToolKitModule import SynonimToolKit
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn import linear_model


class MedicalQuestionClassifier:
    def __init__(self):
        self.hello = "hello"
        self.FF = SynonimToolKit()
        f = open('static/data/vectorizer.pickle', 'rb')
        self.vectorizer = pickle.load(f)
        f.close()

        f = open('static/data/cfier.pickle', 'rb')
        self.cfier = pickle.load(f)
        f.close()

    def predictQuestionType(self,questionString):
        preparedQuestion = self.FF.ReplaceSynonym(questionString,"disease")
        return  self.cfier.predict(self.vectorizer.transform([preparedQuestion]))






