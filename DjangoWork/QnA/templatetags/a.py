from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader
from django.views.decorators.csrf import csrf_protect
from django.http import JsonResponse
from SPARQLWrapper import SPARQLWrapper, JSON
import nltk
from nltk.corpus import wordnet as wn
from nltk.tokenize import sent_tokenize
from nltk.tokenize import word_tokenize
import itertools
from nltk.stem import PorterStemmer
from django import template

# def index(request):
    
#     return render(request, 'index.html')

# def results(request):

# 	if request.method == "POST":
# 		resource = request.POST['question']
# 		words=word_tokenize(resource)
# 		words=[word.lower() for word in words if word.isalpha()]
# 		stopword_list = nltk.corpus.stopwords.words('english')
# 		filtered_tokens = [token for token in words if token not in stopword_list]


# 		sparql = SPARQLWrapper("http://localhost:3030/ds/query")
# 		sparql.setQuery("""
# 		        PREFIX owl: <http://www.w3.org/2002/07/owl#>
# 				PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
# 				PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
# 				PREFIX foaf: <http://xmlns.com/foaf/0.1/>
# 				PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
# 				PREFIX dbpedia: <http://dbpedia.org/ontology/>
# 				PREFIX dbpprop: <http://dbpedia.org/property/>
# 				PREFIX dbpedia-owl: <http://dbpedia.org/ontology/>
# 				PREFIX : <http://www.semanticweb.org/kalani/ontologies/beyond.rdf#>

# 		        SELECT ?x
# 				WHERE {
# 				    ?s rdf:type owl:Class.
#   					?s rdfs:label ?x.
# 				}
# 		""")
# 		sparql.setReturnFormat(JSON)
# 		results = sparql.query().convert()
# 		answer_list = list()
# 		for result in results["results"]["bindings"]:
# 			answer_list.append(result["x"]["value"])						

# 		ps = PorterStemmer()
# 		superClasses= []
# 		for tokens in filtered_tokens:
# 			syn_sets = wn.synsets(tokens)
# 			for syn_set in syn_sets:
# 				hyper=(syn_set.hypernym_paths())
# 				for h in hyper:
# 					for hy in h:
# 						hpr=hy.lemma_names
# 						for superClass in answer_list:
# 							if (superClass in hpr or ps.stem(superClass) in hpr)and ({"token":str(tokens).strip("u"),"superClasses":superClass} not in superClasses):
# 								superClasses.append({"token":str(tokens).strip("u"),"superClasses":superClass})
		
# 		stack=list()	
# 		answer_list2 =list()
# 		dictionary=dict()
				
# 		for s in superClasses:
# 			if s.get('superClasses') == 'disease':
# 				enteredDisease ="'"+s.get("token")+"'"
# 			else:
# 				stack.append(s.get('superClasses'))



# 		for item in stack:
# 			sub_list=list()	
# 			item = ":"+item
# 			sparql2 = SPARQLWrapper("http://localhost:3030/ds/query")
# 			sparql2.setQuery("""
# 			        PREFIX owl: <http://www.w3.org/2002/07/owl#>
# 					PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
# 					PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
# 					PREFIX foaf: <http://xmlns.com/foaf/0.1/>
# 					PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
# 					PREFIX dbpedia: <http://dbpedia.org/ontology/>
# 					PREFIX dbpprop: <http://dbpedia.org/property/>
# 					PREFIX dbpedia-owl: <http://dbpedia.org/ontology/>
# 					PREFIX : <http://www.semanticweb.org/kalani/ontologies/beyond.rdf#>

# 			        SELECT ?b
# 					WHERE {
# 					     ?y owl:someValuesFrom """+item+""".
# 						 ?y owl:onProperty ?x.
# 						 ?z rdf:type :disease.
# 						 ?z rdfs:label """+enteredDisease+""" .
# 						 ?z ?x ?a.
# 						 ?a rdfs:label ?b.
# 					}
# 			""")
# 			sparql2.setReturnFormat(JSON)
# 			results2 = sparql2.query().convert()
# 			for result2 in results2["results"]["bindings"]:
# 				sub_list.append(result2["b"]["value"])
# 			answer_list2.append({item.strip(":"):sub_list})


# 		return render(request, 'answer.html',  {"results" :answer_list2}) 


		# return HttpResponse(superClasses)

		