from django import template
from django.utils.html import escape
from django.utils.safestring import mark_safe

register = template.Library()

def replace(value, arg):
    return value.replace(arg, ' ')