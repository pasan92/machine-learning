from django.conf.urls import include,url
from . import views

urlpatterns = [
    url(r'^questions/', views.index,name="index"),
    url(r'^results/',  views.results, name='results'),
    url(r'^testing/', views.testing, name='testing')
  
]
