$(document).on('submit', "#searchform", function (event) {
  event.preventDefault();
  var ele = $(this);
  $.ajax({
    type: ele.attr("method"),
    url: ele.attr("action"),
    data: ele.serialize(),
    success: function (data) {
       $('#answer').html(data);
    }
  });
});

