import cPickle
import pickle

import FinalFeatureExtract

#load classifier and feature list
f = open('classifier2.pickle', 'rb')
classifier = pickle.load(f)
f.close()
question_features_bag = cPickle.load(open('questionfeaturebag.p', 'rb'))

#find feature function
def find_features(question):
    words = set(question)
    features ={}
    for w in question_features_bag:
        features[w] = (w in words)

    return features

# testquestion = "What causes diabities ?"
# print(classifier.prob_classify( find_features(word_tokenize( testquestion))).prob('DESC'))
# print(classifier.prob_classify( find_features(word_tokenize( testquestion))).prob('DESC'))


testquestion = "what is heart attack"

# print(classifier.classify( find_features(FinalFeatureExtract.Prepare( testquestion))))

# def clasifyquestion(ques):
#     return classifier.classify( find_features(FinalFeatureExtract.Prepare(ques)))

list_of_classes =['ABT_symptom', 'ABT_diseases', 'ABT_prev', 'IS_cau_dis', 'IS_sym_dis', 'ABT_causes', 'IS_prev_dis']


def GiveProbabilities(question):
    lable_text = []
    dis =classifier.prob_classify(find_features(FinalFeatureExtract.Prepare(question)))
    for cls in list_of_classes:
        lable_text.append(cls + " : " + str(round(dis.prob(cls),4)))

    lable_text.append(dis.max())
    return lable_text


# print(str(GiveProbabilities("what is heart attack")))