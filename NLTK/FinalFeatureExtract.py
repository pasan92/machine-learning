from nltk.collocations import BigramCollocationFinder
from nltk.metrics import BigramAssocMeasures
from nltk.tokenize import word_tokenize
from nltk.corpus import wordnet as wn
from nltk.stem import WordNetLemmatizer

lemmatizer = WordNetLemmatizer()
import string
# tokenized_question = word_tokenize("what are the complications arise from heart attack")
# print(tokenized_question)
# bigram_finder = BigramCollocationFinder.from_words(tokenized_question)
# bigrams = bigram_finder.nbest(BigramAssocMeasures.chi_sq,20)










# print(bigrams)
#
# print(bigrams)
list = ['disease','illness','cancer','contamination','defect','disorder','epidemic','fever','flu','illness','sickness','syndrome']
synslist = [str(syn) for w in list  for syn in wn.synsets(w)]
# #list = ['disease','illness','cancer','contamination','defect','disorder','epidemic','illness']
#punch = list(string.punctuation)



def Prepare(strQuestion):
    disease = False
    strQuestion = strQuestion.lower()
    tokanizedQuestion =word_tokenize(strQuestion)
    tokanizedQuestion =[ lemmatizer.lemmatize(w) for w in tokanizedQuestion]
    #tokanizedQuestion = filter(lambda w: not w in punch, tokanizedQuestion)
    tokanizedQuestion.append('diseaseword' if DiseaseCheck(tokanizedQuestion) else '')
    tokanizedQuestion = filter(lambda w:  not w in list,tokanizedQuestion)
    #print(tokanizedQuestion)
    return tokanizedQuestion

def isDiseaseWord(disease):
    hypernamepaths =[ str(hyp) for syns in  wn.synsets(disease) for hypset in syns.hypernym_paths() for hyp in hypset ]
    commonTerms = filter(lambda w:  w in synslist,hypernamepaths)
    return len(commonTerms) >0

def DiseaseCheck(tokanizedQuestion):
    unigram_diseasesList = filter(lambda w:  isDiseaseWord(w),tokanizedQuestion)
    bigram_finder = BigramCollocationFinder.from_words(tokanizedQuestion)
    bigrams = bigram_finder.nbest(BigramAssocMeasures.chi_sq, 20)
    wrd_str = []
    for fst, scnd in bigrams:
        wrd_str.append( fst + '_' + scnd)
    bigrams_diseasesList = filter(lambda w:  isDiseaseWord(w),wrd_str)
    return  (len(bigrams_diseasesList) >0) or (len(unigram_diseasesList)>0)


#print(Prepare("what cause Disease"))