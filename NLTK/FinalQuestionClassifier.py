import cPickle
import pickle
import random

import nltk
from nltk.corpus import stopwords

import FinalFeatureExtract

stopwords = set(stopwords.words("english"))


#seprate class and question
def separater(qString):
    question = []
    pattern = r'''(.+):(.*)'''
    question = nltk.regexp_tokenize(qString, pattern)
    return question[0]


#make a list of questions and classes
questionlist = []
with open('questionlist2.txt') as inputfile:
    for line in inputfile:
        questionlist.append(separater(line.strip()))


tokenized_question_list =[]
#feature list
allwords =[]
for cls,qus in questionlist:
    prepared_question = FinalFeatureExtract.Prepare(qus)
    allwords.extend(prepared_question)
    tokenized_question_list.append([prepared_question,cls])

new_tokanized_question_list =[]
# for wd in tokenized_question_list:
#     if wd not in new_tokanized_question_list:
#         new_tokanized_question_list.append(wd)

new_tokanized_question_list = tokenized_question_list

random.shuffle(allwords)
filteredallwords = nltk.FreqDist(allwords)
# question_features_bag = list(filteredallwords.keys())[:5000]
question_features_bag = set(filteredallwords.keys())
cPickle.dump(question_features_bag, open('questionfeaturebag.p', 'wb'))

# find features in given question.
def find_features(question):
    words = set(question)
    features ={}
    for w in question_features_bag:
        features[w] = (w in words)

    return  features

#prepare the data set for traning and testing
questions_data_set =  [(find_features(question),cls) for (question,cls) in new_tokanized_question_list]
random.shuffle(questions_data_set)

traning_set = questions_data_set[:780]
testing_set = questions_data_set[780:]

classifier = nltk.NaiveBayesClassifier.train(traning_set)
print("Accuracy",(nltk.classify.accuracy(classifier,testing_set))*100)
classifier.show_most_informative_features(15)

f = open('classifier2.pickle', 'wb')
pickle.dump(classifier, f)
f.close()
# cPickle.dump(classifier, open('Primary2.p', 'wb'))


testquestion = "what is heart attack"

print(classifier.classify(find_features(FinalFeatureExtract.Prepare(testquestion))))



#get the set of all the classes
classes =[]
classes = [type for type,question in questionlist]
print(set(classes))

#get list of stop words










# string = "DESC:manner How did serfdom develop in and then leave Russia ?"
# pattern =r'''(.+:)(.*)'''
# print(nltk.regexp_tokenize(string,pattern)[0][0])



