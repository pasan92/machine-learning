import  nltk
from nltk.tokenize import  PunktSentenceTokenizer
from nltk.corpus import state_union

train_text = state_union.raw("2005-GWBust.txt")
sample_text = state_union.raw("2006-GWBust.txt")

custome_sent_tokenizer = PunktSentenceTokenizer(train_text)
Tokenized = custome_sent_tokenizer.tokenize((sample_text))

def process_content():
    try:
        for i in Tokenized:
            words = nltk.word_tokenize(i)
            tagged = nltk.pos_tag(words)
            print (tagged)
    except Exception as e:
        print(str(e))



