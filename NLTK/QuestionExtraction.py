import csv
import  nltk
import random
from nltk.corpus import stopwords
import string
import cPickle
import  pickle
from nltk.tokenize import word_tokenize

stopwords = set(stopwords.words("english"))
punch = list(string.punctuation)

#seprate class and question
def separater(qString):
    question = []
    pattern = r'''(.+):[a-z]+\s(.*)'''
    question = nltk.regexp_tokenize(qString, pattern)
    return question[0]


#make a list of questions and classes
questionlist = []
with open('questionlist.txt') as inputfile:
    for line in inputfile:
        questionlist.append(separater(line.strip()))


tokenized_question_list =[]
#feature list
allwords =[]
for cls,qus in questionlist:
    tokenized_question = word_tokenize(qus)
    tokenized_question = filter(lambda w: (not w in stopwords) and (not w in punch), tokenized_question)
    allwords.extend(tokenized_question)
    tokenized_question_list.append([tokenized_question,cls])

random.shuffle(allwords)
filteredallwords = nltk.FreqDist(allwords)
question_features_bag = list(filteredallwords.keys())[:5000]
cPickle.dump(question_features_bag, open('featurelist.p', 'wb'))

# find features in given question.
def find_features(question):
    words = set(question)
    features ={}
    for w in question_features_bag:
        features[w] = (w in words)

    return  features

#prepare the data set for traning and testing
questions_data_set =  [(find_features(question),cls) for (question,cls) in tokenized_question_list]

traning_set = questions_data_set[:500]
testing_set = questions_data_set[500:]

classifier = nltk.NaiveBayesClassifier.train(traning_set)
print("Accuracy",(nltk.classify.accuracy(classifier,testing_set))*100)
classifier.show_most_informative_features(15)

f = open('Primary.pickle', 'wb')
pickle.dump(classifier, f)
f.close()
# cPickle.dump(classifier, open('Primary2.p', 'wb'))


testquestion = "Who must answer for this ?"

print(classifier.classify( find_features(word_tokenize( testquestion))))



#get the set of all the classes
classes =[]
classes = [type for type,question in questionlist]


#get list of stop words










# string = "DESC:manner How did serfdom develop in and then leave Russia ?"
# pattern =r'''(.+:)(.*)'''
# print(nltk.regexp_tokenize(string,pattern)[0][0])



