import csv
import  nltk
import random
from nltk.corpus import stopwords
import string
import cPickle
import  pickle
import  re
from nltk.tokenize import word_tokenize , sent_tokenize



stopwords = set(stopwords.words("english"))
punch = list(string.punctuation)

#seprate class and question
def separater(qString):
    question = []
    pattern = r'''(.+);;(.*)\n'''
    question = nltk.regexp_tokenize(qString, pattern)
    # print(question)
    return question[0]


#make a list of questions and classes
questionlist = []
with open('symptomsList.txt') as inputfile:
    for line in inputfile:
        #print(line)
        line =re.sub(r'[^\x00-\x7F]+', ' ', line)
        questionlist.append(separater(line))


tokenized_question_list =[]
#feature list
allwords =[]
for cls,qus in questionlist:
    tokenized_question = [word  for send in sent_tokenize(qus) for word in word_tokenize(send)]
    # tokenized_question = word_tokenize( sent_tokenize(qus))
    tokenized_question = filter(lambda w:  not w in punch, tokenized_question)
    allwords.extend(tokenized_question)
    tokenized_question_list.append([tokenized_question,cls])

# random.shuffle(allwords)
# filteredallwords = nltk.FreqDist(allwords)
# # question_features_bag = list(filteredallwords.keys())[:5000]
# question_features_bag = list(filteredallwords.keys())
# cPickle.dump(question_features_bag, open('featurelist2.p', 'wb'))
#
# # find features in given question.
# def find_features(question):
#     words = set(question)
#     features ={}
#     for w in question_features_bag:
#         features[w] = (w in words)
#
#     return  features
#
# #prepare the data set for traning and testing
# questions_data_set =  [(find_features(question),cls) for (question,cls) in tokenized_question_list]
# random.shuffle(questions_data_set)
#
# traning_set = questions_data_set[:75]
# testing_set = questions_data_set[:75]
#
# classifier = nltk.NaiveBayesClassifier.train(traning_set)
# print("Accuracy",(nltk.classify.accuracy(classifier,testing_set))*100)
# classifier.show_most_informative_features(15)
#
# f = open('Primary2.pickle', 'wb')
# pickle.dump(classifier, f)
# f.close()
# # cPickle.dump(classifier, open('Primary2.p', 'wb'))
#
#
# testquestion = "Birth control pills"
#
# print(classifier.classify( find_features(word_tokenize( testquestion))))
#
#
#
# #get the set of all the classes
# classes =[]
# classes = [type for type,question in questionlist]
#
# print(classes)
#
# #get list of stop words
#
#
#
#
#
#
#
#
#
#
# # string = "DESC:manner How did serfdom develop in and then leave Russia ?"
# # pattern =r'''(.+:)(.*)'''
# # print(nltk.regexp_tokenize(string,pattern)[0][0])
#
#

