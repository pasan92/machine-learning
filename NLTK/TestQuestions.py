import  nltk
import random
from nltk.corpus import stopwords
import string
import cPickle
import  pickle

from nltk.tokenize import word_tokenize

import itertools
from nltk.collocations import BigramCollocationFinder
from nltk.metrics import BigramAssocMeasures

#
# def bigram_word_feats(words, score_fn=BigramAssocMeasures.chi_sq, n=10):
#     bigram_finder = BigramCollocationFinder.from_words(words)
#     bigrams = bigram_finder.nbest(score_fn, n)
#     return dict([(ngram, True) for ngram in itertools.chain(words, bigrams)])


stopwords = set(stopwords.words("english"))
punch = list(string.punctuation)
#seprate class and question
def separater(qString):
    question = []
    #pattern = r'''(.+):[a-z]+\s(.*)'''
    pattern = r'''(.+):(.*)'''
    question = nltk.regexp_tokenize(qString, pattern)
    return question[0]


print(separater("AIDS & HIV : While 1.1 million Americans currently live with HIV/AIDS"))

#
#make a list of questions and classes
questionlist = []
with open('symptomsList.txt') as inputfile:
    for line in inputfile:
        questionlist.append(separater(line.strip()))
#
#
#
# tokenized_question_list =[]
# #feature list
# #allwords =[]
#
# for cls,qus in questionlist:
#     tokenized_question = word_tokenize(qus)
#     tokenized_question = filter(lambda w:  (not w in punch), tokenized_question)
#     #allwords.extend(tokenized_question)
#
#     bigram_finder = BigramCollocationFinder.from_words(tokenized_question)
#     bigrams = bigram_finder.nbest(BigramAssocMeasures.chi_sq, 4)
#     bag_of_features = dict([[word, True] for word in bigrams])
#     #print(bigrams)
#    # print(bag_of_features)
#     tokenized_question_list.append([bag_of_features,cls])
#
#
# traning_set = tokenized_question_list[:760]
# testing_set = tokenized_question_list[760:]
#
#
# classifier = nltk.NaiveBayesClassifier.train(traning_set)
# print("Accuracy",(nltk.classify.accuracy(classifier,testing_set))*100)
#
# testquestion = "what is illness"
# bag_of_features = dict([[word, True] for word in word_tokenize(testquestion)])
# print(classifier.classify(  bag_of_features))
# classifier.show_most_informative_features(15)