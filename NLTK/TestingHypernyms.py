from nltk.corpus import wordnet as wn

word = wn.synsets("heart_attack")[0]

print(word)

hypernyms = word.hypernym_paths()

print(hypernyms)