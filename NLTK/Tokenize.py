from nltk.tokenize import sent_tokenize,word_tokenize

example_text ="hello how are you mr. Smith, what are you doing."

print(sent_tokenize(example_text))

for i in word_tokenize(example_text):
    print(i)