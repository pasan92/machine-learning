import cPickle
import pickle

import FinalFeatureExtract

#load classifier and feature list
f = open('classifier2.pickle', 'rb')
classifier = pickle.load(f)
f.close()
question_features_bag = cPickle.load(open('questionfeaturebag.p', 'rb'))

#find feature function
def find_features(question):
    words = set(question)
    features ={}
    for w in question_features_bag:
        features[w] = (w in words)

    return features

testquestion = "What causes diabities ?"
# print(classifier.prob_classify( find_features(word_tokenize( testquestion))).prob('DESC'))
# print(classifier.prob_classify( find_features(word_tokenize( testquestion))).prob('DESC'))


testquestion = "how to check for heart attack"

print(classifier.classify(find_features(FinalFeatureExtract.Prepare(testquestion))))