import nltk
import random
from nltk.corpus import movie_reviews
from nltk.corpus import stopwords
import string

stopwords = set(stopwords.words("english"))

document = [(list(movie_reviews.words(fileid)),category)
            for category in movie_reviews.categories()
            for fileid in movie_reviews.fileids(category)
]

filtereddoc =[]
punch = list(string.punctuation)

for wordlist,cat in document:
    print(type(wordlist))
    wordlist = filter(lambda w: (not w in stopwords) and (not w in punch),wordlist)
    filtereddoc.extend(wordlist)

random.shuffle(filtereddoc)
filteredallwords = nltk.FreqDist(filtereddoc)
word_features = list(filteredallwords.keys())[:5000]


def find_features(document):
    words = set(document)
    features ={}
    for w in word_features:
        features[w] = (w in words)

    return  features

#print((find_features(movie_reviews.words('neg/cv000_29416.txt'))))

features = [(find_features(rev),category) for (rev,category) in document]

traning_set = features[:1900]
testing_set = features[1900:]

classifier = nltk.NaiveBayesClassifier.train(traning_set)
print("Accuracy",(nltk.classify.accuracy(classifier,testing_set))*100)
classifier.show_most_informative_features(15)