import nltk
import random
from nltk.corpus import movie_reviews
from nltk.corpus import stopwords
import string

stopwords = set(stopwords.words("english"))

document = [(list(movie_reviews.words(fileid)),category)
            for category in movie_reviews.categories()
            for fileid in movie_reviews.fileids(category)
]

filtereddoc =[]
punch = list(string.punctuation)
print(punch)

#remove punctuations and stopwords
for wordlist,cat in document:
    wordlist = filter(lambda w: (not w in stopwords) and (not w in punch),wordlist)
    filtereddoc.extend(wordlist)

#shufle the list
random.shuffle(filtereddoc)
#print(len(filtereddoc[0][0]))
#print(len(document[0][0]))

#allwords =[]

#allwords = [w for w,cat in movie_reviews.words()]
filteredallwords = nltk.FreqDist(filtereddoc)
print(filteredallwords.most_common(10))
print(filteredallwords["stupid"])


