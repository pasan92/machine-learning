from owlready import *



onto_path.append("/home/pasan/Documents/MyWorks/machinelearning/Owl")
# onto = get_ontology("http://test.org/Beyon.owl")
onto = get_ontology("http://test.org/MedicalOntology.owl")
# onto = get_ontology("http://test.org/beyond_kalani_inXML.owl")
onto.load()

disease =onto.classes[4]
Symptom = onto.classes[5]

print(disease)
print(Symptom)
for ins in onto.instances:
    print(ins)
# for cls in onto.classes:
#     print (cls)

training_data_path = "dataset_clean1.csv"

def load_data(filename):
    list = []
    with open(filename, 'r') as f:
        for line in f:
            disease, symptoms = line.split(",", 1)
            list.append((disease, symptoms))
    return list


list = load_data(training_data_path)


def FindInstance(insName):
    for individualName in onto.instances:
        if(individualName.name == insName):
            return individualName

def AddSymptom(symptomName,Disease):
    Symp = FindInstance(symptomName)

    if Symp is  None:
        Symp = Symptom(symptomName)
        ANNOTATIONS[Symp].add_annotation(rdfs.label, symptomName)
    Disease.has_symptom.append(Symp)


def AddDis(Disease):
    Symp = FindInstance(Disease)
    if Symp ==  None:
        Symp = disease(Disease)
        ANNOTATIONS[Symp].add_annotation(rdfs.label, Disease)

def addUnderscoreToSPace(sentense):
    tokanized = sentense.split(" ")
    newsentense =""
    num =0
    for word in tokanized:
        if not num ==0:
            newsentense = newsentense +"_"+word
        else:
            newsentense = word
        num = num +1

    return  newsentense

for dis,syn in list:
    if dis != None and syn !=None:
        # AddDis(addUnderscoreToSPace(dis))
        sympt = syn.strip('\n')
        disObj = FindInstance(addUnderscoreToSPace(dis))
        if disObj !="" and disObj !=" " and disObj != None:
            AddSymptom(addUnderscoreToSPace(sympt),disObj)

onto.save()