from owlready import *



onto_path.append("/home/pasan/Documents/MyWorks/machinelearning/Owl")
# onto = get_ontology("http://test.org/Beyon.owl")
onto = get_ontology("http://test.org/MedicalOntology.owl")
# onto = get_ontology("http://test.org/beyond_kalani_inXML.owl")
onto.load()


# for ClassName in onto.classes:
#     print (ClassName)



# Symptom = onto.classes[3]
# Prevention =onto.classes[0]
# Treatment = onto.classes[5]
# risk = onto.classes[6]
# disease =onto.classes[4]





#new medical ontology
casuses = onto.classes[0]
Prevention =onto.classes[1]
disease =onto.classes[2]
risk = onto.classes[3]
Symptom = onto.classes[4]
Treatment = onto.classes[5]
# print(onto.properties)
print(Treatment)


# for individualName in onto.instances:
#     print (individualName.name )

def FindInstance(insName):
    for individualName in onto.instances:
        if(individualName.name == insName):
            return individualName

def AddSymptom(symptomName,Disease):
    Symp = FindInstance(symptomName)

    if Symp is  None:
        Symp = Symptom(symptomName)
        ANNOTATIONS[Symp].add_annotation(rdfs.label, symptomName)
    Disease.has_symptom.append(Symp)



def AddPrevention(Name,Disease):
    AddingClass = FindInstance(Name)

    if AddingClass is None:
        AddingClass = Prevention(Name,lable =Name)
        ANNOTATIONS[AddingClass].add_annotation(rdfs.label, Name)
    Disease.can_prevent_by.append(AddingClass)

def AddTreatment(Name,Disease):
    AddingClass = FindInstance(Name)
    if AddingClass is None:
        AddingClass = Treatment(Name,label =Name)
        ANNOTATIONS[AddingClass].add_annotation(rdfs.label, Name)

    Disease.has_treatment.append(AddingClass)

def AddRisk(Name,Disease):
    AddingClass = FindInstance(Name)
    if AddingClass is None:
        AddingClass = risk(Name,label = Name)
        ANNOTATIONS[AddingClass].add_annotation(rdfs.label, Name)
    Disease.has_risk.append(AddingClass)

def AddCauses(Name,Disease):
    AddingClass = FindInstance(Name)
    if AddingClass is None:
        AddingClass = casuses(Name,label = Name)
        ANNOTATIONS[AddingClass].add_annotation(rdfs.label, Name)
    Disease.has_cause.append(AddingClass)




def addUnderscoreToSPace(sentense):
    tokanized = sentense.split(" ")
    newsentense =""
    num =0
    for word in tokanized:
        if not num ==0:
            newsentense = newsentense +"_"+word
        else:
            newsentense = word
        num = num +1

    return  newsentense

def AddInfoToDis(Disease,Symp_list,Prevent_list,Treatment_list,Risk_list,EarlySymp_list,Causes_list):
    for item in Symp_list:
        item = addUnderscoreToSPace(item)
        AddSymptom(item,Disease)
    for item in EarlySymp_list:
        item = addUnderscoreToSPace(item)
        AddSymptom(item,Disease)
    for item in Prevent_list:
        item = addUnderscoreToSPace(item)
        AddPrevention(item,Disease)
    for item in Treatment_list:
        item = addUnderscoreToSPace(item)
        AddTreatment(item,Disease)
    for item in Risk_list:
        item = addUnderscoreToSPace(item)
        AddRisk(item,Disease)
    for item in Causes_list:
        item = addUnderscoreToSPace(item)
        AddCauses(item,Disease)




# dis =FindInstance("arthritis")
# Symp_list =["Pain","Swelling",'Stiffness','difficulty moveing a joing']
# EarlySymp_list =["Pain"]
# Prevent_list =["excercise"]
# Treatment_list =["Medications","non-pharamacologic therapies",'splints or join assistive aid','weight loss','surgery']
# Risk_list = ["non-fatal"]
# Causes_list =["Injury","abnormal metabolism","Inheritance","Infections","Immune system dysfunction"]
#
#
# AddInfoToDis(dis,Symp_list,Prevent_list,Treatment_list,Risk_list,EarlySymp_list,Causes_list)



# dis = disease("chickenpox",)
# ANNOTATIONS[dis].add_annotation(rdfs.label, "chickenpox")
#
# Symp_list =["headache","sore throat,",'Stiffness','stomachache',"itchy skin",'red spots']
# EarlySymp_list =["fever"]
# Prevent_list =["vacancies"]
# Treatment_list =["vacancies"]
# Risk_list = ["non-fatal"]
# Causes_list =["virus","varicella-zoster"]
# AddInfoToDis(dis,Symp_list,Prevent_list,Treatment_list,Risk_list,EarlySymp_list,Causes_list)
#
# dis =FindInstance("Cholera")
#
#
#
# dis = disease("cholera",)
# ANNOTATIONS[dis].add_annotation(rdfs.label, "cholera")
#
# Symp_list =["diarrhea","vomitting",'dehydration','rapid heart rate',"itch skin",'dry mucus membrains','low blood pressure']
# EarlySymp_list =["fever"]
# Prevent_list =["Drinking",'Preparing clean food or drinks','Wash hands before eat','Wash food before preparing']
# Treatment_list =["Medicine"]
# Risk_list = ["can be fatal"]
# Causes_list =["municipal water supplies","ice from municiple water","food and drinks sold by streen vendors","raw or undercooked fish or sea food"]
# AddInfoToDis(dis,Symp_list,Prevent_list,Treatment_list,Risk_list,EarlySymp_list,Causes_list)
#
#
# dis = disease("dengue",)
# ANNOTATIONS[dis].add_annotation(rdfs.label, "dengue")
# description = "Dengue fever is caused by any one of four dengue viruses spread by mosquitoes that thrive in and near human lodgings. When a mosquito bites a person infected with a dengue virus, the virus enters the mosquito. When the infected mosquito then bites another person, the virus enters that person's bloodstream."
# dis.description.append(description)
#
# Symp_list =["headache",'eye_pain','join_pain',"mussle_pain",'bone_pain','rash','mild_bleeding']
# EarlySymp_list =["fever"]
# Prevent_list =["reduce mosquito habitat","wear protective clothing","stay in air-conditioned owell-screened housing","mosquito repellents"]
# Treatment_list =["symptomatic_treatment","treatmenttowards relief of symptoms"]
# Risk_list = ["can be fatal"]
# Causes_list =["virues",'mosquitoes','']
# AddInfoToDis(dis,Symp_list,Prevent_list,Treatment_list,Risk_list,EarlySymp_list,Causes_list)
#
# name = "common_cold"
#
# dis = disease(name)
# ANNOTATIONS[dis].add_annotation(rdfs.label, name)
# description = "Symptoms of a common cold usually appear one to three days after exposure to a cold-causing virus."
# dis.description.append(description)
#
# Symp_list =["runny or stuffy nose",'sore throat',"Congestion",'body aches','headaches','sneezing','fever','malaise']
# EarlySymp_list =["Cough"]
# Prevent_list =['wash hands','disinfest your stuff','use tissues','dont share personal items']
# Treatment_list =["symptomatic_treatment","treatmenttowards relief of symptoms"]
# Risk_list = ["fatal for weak immune system"]
# Causes_list =["virus",'rhinoviruses','virus enter the body through mouth, eyes or nose']
# AddInfoToDis(dis,Symp_list,Prevent_list,Treatment_list,Risk_list,EarlySymp_list,Causes_list)
#
# name = "smallpox"
#
# dis = disease(name)
# ANNOTATIONS[dis].add_annotation(rdfs.label, name)
# description = "Smallpox was an infectious disease caused by either of two virus variants, Variola major and Variola minor.[3] The disease is also known by the Latin names variola or variola vera, derived from varius or varus . "
# dis.description.append(description)
#
# Symp_list =['fever','chills','server back pain','abdomainal pain','vomitting']
# EarlySymp_list =['headache']
# Prevent_list =['no medicine to prevent smallpox']
# Treatment_list =["No known treatment is effective for smallpox",'patient should be isolated untill all scapbs have fallen off','fluid and electrolyte balance','medicines shoud be given to fever and pain']
# Risk_list = ["fatal"]
# Causes_list =["infection of variola virus"]
# AddInfoToDis(dis,Symp_list,Prevent_list,Treatment_list,Risk_list,EarlySymp_list,Causes_list)
#
# name = "rabies"
#
# dis = disease(name)
# ANNOTATIONS[dis].add_annotation(rdfs.label, name)
# description = "Smallpox was an infectious disease caused by either of two virus variants, Variola major and Variola minor.[3] The disease is also known by the Latin names variola or variola vera, derived from varius or varus . "
# dis.description.append(description)
#
# Symp_list =['fever','headache','nausea','vomitting','agitation','anziety','hyperactivity']
# EarlySymp_list =['confusion']
# Prevent_list =['vaccinates dogs and cats','keep dogs and cats under control','stay away from unknown cats and dogs','do not keep wild animals as pets']
# Treatment_list =['tsnird immune globulin shot to prevent infecting']
# Risk_list = ["fatal"]
# Causes_list =["rabies virus",'saliva of infected nimals','biting']
# AddInfoToDis(dis,Symp_list,Prevent_list,Treatment_list,Risk_list,EarlySymp_list,Causes_list)
#
# name = "sleeping_sickness"
#
# dis = disease(name)
# ANNOTATIONS[dis].add_annotation(rdfs.label, name)
# description = "Sleeping sickness is infection with germs carried by certain flies. It results in swelling of the brain."
# dis.description.append(description)
#
# Symp_list =['drowsiness during the day','fever','headache','insomnai at night','mood changes','sleepiness','sweationg','swollen lymph nodes all over the body','red painfull nodules at site of gly bite','weakness'
#                                                                                                                                                                                                                     '']
# EarlySymp_list =['anixiety']
# Prevent_list =['pentamidine injection']
# Treatment_list =['eflornthine','melarsoprol','pentamidine','siramin']
# Risk_list = ["fatal"]
# Causes_list =["germs",'ttypansoma brucei rhodesinse','trypanosoma brucei gambinse','bitten by tsets flies']
# AddInfoToDis(dis,Symp_list,Prevent_list,Treatment_list,Risk_list,EarlySymp_list,Causes_list)
#
# name = "malaria"
#
# dis = disease(name)
# ANNOTATIONS[dis].add_annotation(rdfs.label, name)
# description = ["Malaria is a parasitic disease that involves high fevers, shaking chills, flu-like symptoms, and anemia"]
# dis.description.append(description)
#
# Symp_list =['bloody stools','chills','coma','convulsion','fever','headache','jandice','muscle pain','nausea','sweating','vomiting']
#
# EarlySymp_list =['anemia']
# Prevent_list =['anti mlaria medicines','immunity due to location']
# Treatment_list =['atovaquone plus proguanil','combination of pyrimethamine and sulfadoxine']
# Risk_list = ["fatal"]
# Causes_list =['parasites','bite of infected anopeeles mosquitoes']
# AddInfoToDis(dis,Symp_list,Prevent_list,Treatment_list,Risk_list,EarlySymp_list,Causes_list)
#
#
# name = "lung_cancer"
#
# dis = disease(name)
# ANNOTATIONS[dis].add_annotation(rdfs.label, name)
# description = ['Lung cancer is a type of cancer that begins in the lungs. Your lungs are two spongy organs in your chest that take in oxygen when you inhale and release carbon dioxide when you exhale.Lung cancer is the leading cause of cancer deaths in the United States, among both men and women']
# dis.description.append(description)
#
# Symp_list =['coughing up blood','breathing difficulties','loss of appetite','fatigue','recurring infections','weight loss','pain in bones'];
# EarlySymp_list =['coughing']
# Prevent_list =['stop smoking','avoid second hand smoking','test your home for randon','avoid cartinogens at work','eat a diet full of fruits and vegitables','exercise most days of week']
# Treatment_list =['Surgery','Chemotherapy','Radiation therapy','Targeted drug therapy','Clinical trials','Palliative care']
# Risk_list = ["fatal"]
# Causes_list =['smoking','exposing to secondhand smoke']
# AddInfoToDis(dis,Symp_list,Prevent_list,Treatment_list,Risk_list,EarlySymp_list,Causes_list)
#
#
# onto.save()
#
#
#
# name = "bronchiectasis"
#
# dis = disease(name)
# ANNOTATIONS[dis].add_annotation(rdfs.label, name)
# description = ['Bronchiectasis is a condition where the bronchial tubes of your lungs are permanently damaged, widened, and thickened. These damaged air passages allow bacteria and mucus to build up and pool in your lungs.']
# dis.description.append(description)
#
# Symp_list =['chronic daily cough','abnormal sounds or wheezing in the chest with breathing','coughing up blood','shortness of breath','chest pain','coughing up large amounts of thick mucus every day'];
# EarlySymp_list =['coughing']
# Prevent_list =['Avoiding toxic fumes','Childhood vaccines for measles and whooping cough']
# Treatment_list =['methods for clearing the airways','pulmonary rehabilitation','bronchodilators like albuterol (Proventil) and tiotropium (Spiriva) to open up airways','medications to thin mucus','vaccinations to prevent respiratory infections','oxygen therapy','vaccinations to prevent respiratory infections']
# Risk_list = ["fatal"]
# Causes_list =['an abnormally functioning immune system','inflammatory bowel disease','autoimmune diseases','COPD','COPD','allergic aspergillosis']
# AddInfoToDis(dis,Symp_list,Prevent_list,Treatment_list,Risk_list,EarlySymp_list,Causes_list)

onto.save()