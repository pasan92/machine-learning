import pandas as pd
import quandl
import math,datetime
import  numpy as np
from sklearn import  preprocessing,svm
from sklearn.linear_model import  LinearRegression
from sklearn.model_selection import train_test_split
import  matplotlib.pyplot as plt
from matplotlib import  style
import pickle

style.use('ggplot')

df = quandl.get('WIKI/GOOGL')

df = df[['Adj. Open','Adj. High','Adj. Low','Adj. Close','Adj. Volume']]
df['HL_PCT']= ((df['Adj. High'] - df['Adj. Close']) /df['Adj. Close'])*100.0
df['PCT_change'] = ((df['Adj. Close'] - df['Adj. Open'])/ df['Adj. Open']) *100.0
df = df[['Adj. Close','HL_PCT','PCT_change','Adj. Volume']]



# print(df.head())

forcast_col = 'Adj. Close'
df.fillna(-9999,inplace=True)

forcast_out = int(math.ceil(0.005*len(df)))

df['label'] = df[forcast_col].shift(-forcast_out)


X = np.array(df.drop(['label'],1))
X = preprocessing.scale(X)
X_lately = X[-forcast_out:]
X = X[:-forcast_out]

df.dropna(inplace=True)
y = np.array(df['label'])




# X = X[:-forcast_out +1]
# df.drop(inplace=True)
# y = np.array(df['Label'])

# X_train,X_test,y_train,y_test = cross_validation.train_test_split(X,y,test_size=0.2)
X_train,X_test,y_train,y_test = train_test_split(X,y,test_size=0.4, random_state=0)

## TRANING PART
# clf = LinearRegression()
# clf.fit(X_train,y_train)
#
# with open('linearregression.pickle','wb') as f:
#     pickle.dump(clf,f)

pickle_in = open('linearregression.pickle','rb')
clf = pickle.load(pickle_in)

accuracy = clf.score(X_test,y_test)

forcast_set = clf.predict(X_lately)
# print(forcast_set,accuracy,forcast_out)

# print(X[33],y[33])
# print("predicted" + str(forcast_set)

df['Forcast'] = np.nan



# first_date = df.iloc[0].name
# first_unix = first_date.timestamp()
# one_day = 86400z
# next_unix = first_unix + one_day

last_date = df.iloc[-1].name
last_unix = last_date.timestamp()
one_day = 86400
next_unix = last_unix + one_day




# for i in forcast_set:
#     next_date = datetime.date.fromtimestamp(next_unix)
#     next_unix += one_day
#     df.loc[next_date] = [np.nan for _ in range (len(df.columns)-1)+[i]]





for i in forcast_set:
    next_date = datetime.date.fromtimestamp(next_unix)
    next_unix += one_day
    df.loc[next_date] = [np.nan for _ in range(len(df.columns)-1)]+[i]


# for i in forcast_set:
#     next_date = datetime.date.fromtimestamp(next_unix)
#     next_unix += one_day
#     df.loc[next_date,'Forcast'] = [i]

# print(df[-40:-30])

df['Adj. Close'].plot()
df['Forcast'].plot()
plt.legend(loc=4)
plt.xlabel('Date')
plt.ylabel('Price')
plt.show()