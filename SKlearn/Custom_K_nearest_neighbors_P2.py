import numpy as np
import matplotlib.pyplot as plt
from matplotlib import style
import warnings
from collections import Counter
import pandas as pd
import random

style.use('fivethirtyeight')


# dataset = {'k':[[1,2],[2,3],[3,1]], 'r':[[6,5],[7,7],[8,6]]}
# new_features = [5,7]
#
# # [[plt.scatter(ii[0],ii[1],s=100,color=i) for ii in dataset[i]] for i in dataset]
# # plt.scatter(new_features[0], new_features[1], s=100)
# # plt.show()
#
# [plt.scatter(ii[0],ii[1],s=100,color=i) for i in dataset for ii in dataset[i]]
# plt.scatter(new_features[0],new_features[1],s =100)
# plt.show()

#K should be learger than total number of classes
def k_nearest_neighbors(data,predict,k=3):
    if len(data) >=k:
        warnings.warn('K is set to a value less thant total voting groups!')

    distances =[]

    for group in data:
        for features in data[group]:
            eulidean_distance = np.linalg.norm(np.array(features) -np.array(predict))
            distances.append([eulidean_distance,group])

    votes = [i[1] for i in sorted(distances)[:k]]
    most_common = Counter(votes).most_common(1)
    confidance =float( most_common[0][1]/k)
    vote_result =most_common[0][0]

    return  vote_result,confidance

df = pd.read_csv('K_Neigh.txt')
#replace missing values (values with ?) with very large value so that they can be included.
#if data with missing values are removed you will loose most of the data
df.replace('?',-99999,inplace=True)
#remove the id colomn
df.drop(['id'],1,inplace=True)
#some data appears as string. this convert them to float
full_data =df.astype(float).values.tolist()
random.shuffle(full_data)

#pecentage of the testing set
test_size = 0.2

#traning and testing dictionaries
train_set ={2:[],4:[]}
test_set ={2:[],4:[]}

#divide in to traning and testing set
train_data = full_data[:-int(test_size*len(full_data))]
test_data = full_data[-int(test_size*len(full_data)):]

#filling to traning and testing dictonary
#append the value to the respected class, class is taken from i[-1] which is the last value of the data
#when data is inserted class is removed, that is why i[:-1].
for i in train_data:
    train_set[i[-1]].append(i[:-1])

for i in test_data:
    test_set[i[-1]].append(i[:-1])


#calcualting the accuracy
correct =0
total =0
for group in test_set:
    for data in test_set[group]:
        vote,confidance = k_nearest_neighbors(train_set,data,k=5)
        if vote == group:
            correct +=1
        else:
            print(confidance)
        total +=1

print('Accuracy: ',correct/total)
