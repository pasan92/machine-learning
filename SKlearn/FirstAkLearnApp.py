import matplotlib.pyplot as plt
from sklearn import datasets
from sklearn import svm

digits = datasets.load_digits()

clf = svm.SVC(gamma=0.001, C=100)

x,y = digits.data[:-1],digits.target[:-1]
print(len(x))
clf.fit(x,y)
print("Prediction:",clf.predict(digits.data[-2])[0])

plt.imshow(digits.images[-2],cmap=plt.cm.gray_r,interpolation="nearest")

# plt.imshow(digits.images[-1])
plt.show()

# import matplotlib.pyplot as plt
#
# from sklearn import datasets
# from sklearn import svm
#
# digits = datasets.load_digits()
#
#
# classifier = svm.SVC(gamma=0.4, C=100)
# x, y = digits.images, digits.target
#
# #only reshape X since its a 8x8 matrix and needs to be flattened
# n_samples = len(digits.images)
# x = x.reshape((n_samples, -1))
# print("before reshape:" + str(digits.images[0]))
# print("After reshape" + str(x[0]))
#
#
# classifier.fit(x[:-2], y[:-2])
# ###
# print('Prediction:', classifier.predict(x[-2]))
# ###
# plt.imshow(digits.images[-2], cmap=plt.cm.gray_r, interpolation='nearest')
# plt.show()
#
# ###
# print('Prediction:', classifier.predict(x[-1]))
# ###
# plt.imshow(digits.images[-1], cmap=plt.cm.gray_r, interpolation='nearest')
# plt.show()