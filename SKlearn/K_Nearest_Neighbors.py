import numpy as np
from sklearn import  preprocessing,neighbors
import  pandas as pd
from sklearn.model_selection import train_test_split


df = pd.read_csv('K_Neigh.txt')
#replace missing values (values with ?) with very large value so that they can be included.
#if data with missing values are removed you will loose most of the data
df.replace('?',-99999,inplace=True)
#remove the id colomn
df.drop(['id'],1,inplace=True)


#X is the feature set, that must not include the class column
X = np.array(df.drop(['class'],1),dtype=np.float64)
#y is the class set
y = np.array(df['class'],dtype=np.float64)

#split in to traning and testing data and randomize them
X_train,X_test,y_train,y_test = train_test_split(X,y,test_size=0.2,random_state=42)



clf = neighbors.KNeighborsClassifier()
clf.fit(X_train,y_train)

accuracy = clf.score(X_test,y_test)
print(accuracy)

example_measures = np.array([[4,2,1,1,1,2,3,2,1],[4,2,1,1,1,1,3,2,1]])
example_measures = example_measures.reshape(2,-1)
print(example_measures)

prection = clf.predict(example_measures)
print(prection)