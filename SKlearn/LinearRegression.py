from statistics import  mean
import numpy as np
import  matplotlib.pyplot as plt
from matplotlib import  style

style.use('fivethirtyeight')

xs =np.array([1,2,3,4,5,6],dtype=np.float64)
ys =np.array([5,4,6,5,6,7],dtype=np.float64)



def best_fit_slope_and_intercept(xs,ys):
    m = ( (mean(xs)*mean(ys)) -mean(xs*ys) ) / ( mean(xs)*mean(xs)- mean(xs*xs) )
    b = mean(ys) -m*mean(xs)
    return m ,b

m,b =best_fit_slope_and_intercept(xs,ys)

regression_line =[(m*x) +b for x in xs]

def squred_error(ys_origin,ys_line):
    return  sum((ys_line-ys_origin)**2)

def coefficent_of_determination(ys_ori,ys_line):
    y_mean_line =[mean(ys_ori) for y in ys_ori]
    squired_error_regr = squred_error(ys_ori,ys_line)
    squared_error_y_mean = squred_error(ys_ori,y_mean_line)
    return  1 - (squired_error_regr/squared_error_y_mean)

def second_squred_r_method(y_reg,y_original):
    y_mean_line = np.array([ mean(y_original) for y in y_original],dtype=np.float64)
    y_reg_err = squred_error(y_reg,y_mean_line)
    y_orig_err = squred_error(y_original,y_mean_line)
    return (y_reg_err/y_orig_err)

r_squre = coefficent_of_determination(ys,regression_line)
r_squre2 =second_squred_r_method(regression_line,ys)
print(r_squre2)
print(r_squre)

# print(m,b)
plt.scatter(xs,ys)
plt.plot(xs,regression_line)
plt.show()