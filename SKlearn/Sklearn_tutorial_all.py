import pandas
import os
import time
from datetime import  datetime
import re
from time import mktime
import  matplotlib
import  matplotlib.pyplot as plt
from matplotlib import  style
style.use("dark_background")

path ="/home/pasan/Documents/TestingData/intraQuarter"

def Key_Stats(gather="Total Debt/Equity [/(]mrq[/)]"):
    statspath = path + '/_KeyStats'
    stock_list = [X[0] for X in os.walk(statspath)]
    df = pandas.DataFrame(columns=['Date',
                                   'Unix',
                                   'Tiker',
                                   "De Ratio",
                                   'Price',
                                   'stock_p_change',
                                   'SP_500',
                                   'sp500_p_change',
                                   'Difference',
                                   'Status'])
    sp500_df = pandas.DataFrame.from_csv("YAHOO-INDEX_GSPC.csv")
    tiker_list = []
    i =0
    print(type(stock_list))
    totalList = len(stock_list)
    for dir in stock_list[1:50]:
        i +=1
        print(str(i) +'/'+str(totalList))

        tiker = dir.split("/")[-1]

        tiker_list.append(tiker)

        starting_stock_value = False
        starting_sp500_value = False

        each_file =os.listdir(dir)
        if len(each_file) >0:
            for file in each_file:
                date_stamp = datetime.strptime(file,"%Y%m%d%H%M%S.html")
                unix_time = time.mktime(date_stamp.timetuple())
                full_file_path = dir +'/' +file
                source = open(full_file_path,'rb').read()
                # print(full_file_path)
                value =0
                # source = "Total Debt/Equity (mrq):dfdf sdsdsd"
                # value = re.match(r'(.*?)Total Debt/Equity [/(]mrq[/)]:(.*)', str(source), re.M | re.I)
                # value = re.match(r'(.*?)Total Debt/Equity [/(]mrq[/)]:</td><td class="yfnc_tabledata1">(.*?)</td>', str(source), re.M | re.I)
                # print(source)
                # value = source.split(gather +':</td><td class="yfnc_tabledata1">')[1].split('</td>')[0]
                try:
                    value = re.match(r'(.*?)Total Debt/Equity [/(]mrq[/)]:</td>\n?<td class="yfnc_tabledata1">(.*?)</td>',str(source), re.M | re.I)
                    value2 = value
                    value = float(value.group(2))
                    # value = value.group(2)
                    # if str(value.group(2)) != "N/A":
                    #     print(file , value.group(2))

                    try:
                        sp500_date = datetime.fromtimestamp(unix_time).strftime('%Y-%m-%d')
                        row = sp500_df[(sp500_df.index ==sp500_date)]
                        sp500_value = float(row["Adjusted Close"])


                    except:
                        sp500_date = datetime.fromtimestamp(unix_time -259200).strftime('%Y-%m-%d')
                        row = sp500_df[(sp500_df.index ==sp500_date)]
                        sp500_value = float(row["Adjusted Close"])


                    # stock_price = source.split('</small><big><b>')[1].split('</b></big>')[0]
                    stock_price = re.match(r'(.*?)/small><big><b>(.*?)</b></big>',str(source), re.M | re.I).group(2)

                    try:
                        filtered_sp = re.match(r'<span id="(.*?)">(.*?)</span>', str(stock_price), re.M | re.I)
                        stock_price = filtered_sp.group(2)

                    except Exception as e:
                        pass

                    stock_price = float(stock_price)
                    # try:
                    #     filtered_sp = re.match(r'<span id="yfs_l10_hig">(.*?)</span>', str(stock_price), re.M | re.I)
                    #     stock_price = filtered_sp.group(1)
                    #     print(filtered_sp.group(1))
                    # except Exception as e:
                    #     pass

                    if not starting_stock_value:
                        starting_stock_value = stock_price

                    if not starting_sp500_value:
                        starting_sp500_value = sp500_value


                    stock_p_change = ((stock_price - starting_stock_value)/starting_stock_value )*100
                    sp500_p_change = ((sp500_value - starting_sp500_value) / starting_sp500_value) * 100
                    difference = stock_p_change - sp500_p_change
                    if difference >0:
                        status ="outperfrom"
                    else:
                        status ="underperfrom"

                    df = df.append({'Date':date_stamp,
                                    'Unix':unix_time,
                                    'Tiker':tiker,
                                    'De Ratio':value,
                                    'Price':stock_price,
                                    'stock_p_change':stock_p_change,
                                    'SP_500':sp500_value,
                                    'sp500_p_change':sp500_p_change,
                                    'Difference':difference,
                                    'Status':status,
                                    },ignore_index=True)




                except Exception as e:
                        pass


    for each_tiker in tiker_list:
        try:
            plot_df = df[(df['Tiker']== each_tiker)]
            plot_df = plot_df.set_index(['Date'])

            if plot_df['Status'][-1] =="underperfrom":
                color ='r'
            else:
                color = 'g'

            plot_df['Difference'].plot(label= each_tiker,color=color)
            plt.legend()
        except:
            pass

    plt.show()

    save = 'datafile.csv'
    print(save)
    df.to_csv(save)


                # print(tiker + ":",value)
                # print(date_stamp,unix_time).split('</td>')[0]
                # time.sleep(60)
    # for s in stock_list:
    #     print(s+ "\n")

Key_Stats()