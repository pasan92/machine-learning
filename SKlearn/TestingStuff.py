#
#
# list = ['hello','world','how','are','you']
#
# for l,index in enumerate(list):
#     print(index)
#     if l is 2:
#         break

# TESTING BeautifulSoup

# from bs4 import  BeautifulSoup
#
#
#
# full_file_path ='/home/pasan/Documents/TestingData/index.txt'
# html_doc = open(full_file_path, 'r')
# soup = BeautifulSoup(html_doc,'lxml')
#
# print(soup)

# TESTING Some Reg

# import  re
#
# source = '<td class="yfnc_tablehead1" width="75%">Total Debt/Equity (mrq):</td>\n<td class="yfnc_tabledata1">0</td>'
# value = re.match(r'(.*?)Total Debt/Equity [/(]mrq[/)]:</td>/n?<td class="yfnc_tabledata1">(.*?)</td>', str(source),re.M | re.I)
# print(value.group(2))


#Testing Numpy

import  numpy as np
# 0,1,2,3,4
# nums = range(5)
#
# selectedNums = nums[-1:]
#
# for num in selectedNums:
#     print(num)

#Testing Pandas
import  pandas as pd

# df1 = pd.DataFrame(np.random.randn(6, 4),
#                       index=list('abcdef'),
#                   columns=list('ABCD'))
# # print(df1)
# print(df1.loc['a'])
#
# df1.loc['a'] = [np.nan for _ in range(len(df1.columns)-1)]+[10]
#
# print([np.nan for _ in range(len(df1.columns)-1)])
#

#Testing Max
data_dict = {-1: np.array([[1, 7],
                           [2, 8],
                           [3, 8], ]),

             1: np.array([[5, 1],
                          [6, -1],
                          [7, 3], ])}


all_data = []

for yi in data_dict:
    for featureset in data_dict[yi]:
        for feature in featureset:
            all_data.append(feature)

max_feature_value = max(all_data)
smin_feature_value = min(all_data)

print(max_feature_value)