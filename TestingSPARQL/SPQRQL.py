from SPARQLWrapper import SPARQLWrapper, JSON

sparql = SPARQLWrapper("http://67.207.85.162:3030/ds/query")
sparql.setQuery("""

PREFIX base:<http://test.org/BeyonUpload.owl#>
PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#>
PREFIX  rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX hsp:<http://www.semanticweb.org/gayantha/ontologies/2017/1/untitled-ontology-15#>

SELECT  ?classLabel
WHERE {
   ?Matchinstance rdf:type ?Class.
     ?Matchinstance rdfs:label ?strLabel.
     ?Class rdfs:label ?classLabel.
FILTER regex(?strLabel, "chickenpox", "i" ).
}
LIMIT 25
""")
sparql.setReturnFormat(JSON)
results = sparql.query().convert()


for result in results["results"]["bindings"]:
    value =(result["classLabel"]['value'])
    print(type(value.encode('utf-8')))

# def symp():
#     value =[]

#
#     print (len(value))
#     print(value)
#     # if(value == None):
#     #     return False,value
#     # else:
#     #     return True,value



