sym_list=[{'symptom': ['excessive_thirst', 'unexplained_weight_loss', 'increased_hunger', 'increase_in_blood_sugar', 'blurred_vision', 'increased_urination', 'sores_that_do_not_heal', 'numbness_or_tingling_in_the_feet_or_hands']},
{'cause': ['Overweight', 'obesity', 'damage_to_or_removal_of_the_pancreas', 'physical_inactivity']}]

sym_list = [v.replace("_", " ") for alist in sym_list for val in alist for v in alist[val]]

print (sym_list)