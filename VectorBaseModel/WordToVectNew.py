import os
import pandas as pd
import nltk
import gensim
from gensim import corpora, models, similarities


# df = pd.read_csv('jokes.csv');
#
# x = df['Question'].values.tolist()
# y = df['Answer'].values.tolist()
#
# corpus = x + y


corpus  =[]

with open('data/new.txt', 'r') as f:
    for line in f:
        corpus.append(nltk.word_tokenize(line))

f.close()



# tok_corp = [nltk.word_tokenize(sent.decode('utf-8')) for sent in corpus]
#
#
# print(tok_corp)
model = gensim.models.Word2Vec(corpus, min_count=1, size=4)

model.save('testmodel')

model = gensim.models.Word2Vec.load('testmodel')
print(model.most_similar('fractures'))
print( model.similarity('fractures', 'symptom'))