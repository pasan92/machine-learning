from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.feature_extraction.text import TfidfVectorizer
from nltk.tokenize import sent_tokenize,word_tokenize

train_set = [
  "This cricket is orange",
    "This orange cricket is orange cricket",
    "This orange is orange orange",
    "This cricket eat orange orange",
    "This is cricket of cricket",
    "This is an orange orange",
    "This cricket is in the cricket ground playing cricket with cricket team",

]

tokanized_list= [word_tokenize(line) for line in train_set]

print(tokanized_list)


#---Count Vectorizer - converting text to vector - VBM ( vector base model)--------------------------
#WITHOUT BIGRAMS

# vectorizer = CountVectorizer()
# x =vectorizer.fit_transform(train_set)
# # print vectorizer
# print (vectorizer.get_feature_names())
# print (x.toarray())
#
# print("new one")
# print(vectorizer.transform(["orange an apple"]).toarray())
# print (vectorizer.get_feature_names())


#WITH BIGRAMS
#Represent Documents in vector from, Features are words and bigrams.
# bigram_vectorizer = CountVectorizer(ngram_range=(1, 2),
#                                     token_pattern=r'\b\w+\b', min_df=1)
# x = bigram_vectorizer.fit_transform(train_set)
# print (bigram_vectorizer.get_feature_names())
#
# print (x.toarray())


#--- Term frequance * Inverse document frequancey --------------------------

#with CountVectorizer
# vectorizer = CountVectorizer()
# x =vectorizer.fit_transform(train_set)
# # print vectorizer
# #print vectorizer.get_feature_names()
# #print x.toarray()
# counts =x.toarray()
#
# transformer = TfidfTransformer(smooth_idf=False)
# tfidf = transformer.fit_transform(counts)
#
# #print tfidf.toarray()
#
# print vectorizer.get_feature_names()
# print transformer.idf_

# Using TfidfVectorizer
# vectorizer =TfidfVectorizer(min_df=1,ngram_range=(1, 2),token_pattern=r'\b\w+\b')
vectorizer =TfidfVectorizer(min_df=1)
x =vectorizer.fit_transform(train_set)
print (vectorizer.get_feature_names())
print (x.toarray())